from setuptools import setup

setup(
    name='python-valr',
    version='0.1.0',
    description='Python client for the VALR API',
    author='DaSagaContinues',
    author_email='Delta@DaSagaContinues.com',
    url='https://gitlab.com/DaSagaContinues/python-valr',
    packages=['valr'],
    install_requires=['requests'],
)
