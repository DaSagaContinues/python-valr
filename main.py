import requests
import json
import hmac
import hashlib
import time

class VALRClient:
    BASE_URL = 'https://api.valr.com/v1/'

    def __init__(self, api_key, api_secret):
        self.api_key = api_key
        self.api_secret = api_secret

    def get_signature(self, path, timestamp, method, data):
        message = f'{path}{timestamp}{method}{data}'.encode('utf-8')
        secret = self.api_secret.encode('utf-8')
        signature = hmac.new(secret, message, hashlib.sha512).hexdigest()
        return signature

    def get_headers(self, path, method, data=None):
        timestamp = str(int(time.time() * 1000))
        signature = self.get_signature(path, timestamp, method, data)
        headers = {
            'X-VALR-API-KEY': self.api_key,
            'X-VALR-SIGNATURE': signature,
            'X-VALR-TIMESTAMP': timestamp,
        }
        if method == 'POST':
            headers['Content-Type'] = 'application/json'
        return headers

    def make_request(self, endpoint, method='GET', data=None):
        url = self.BASE_URL + endpoint
        headers = self.get_headers(endpoint, method, data)
        response = requests.request(method, url, headers=headers, json=data)
        if response.status_code == 200:
            return json.loads(response.text)
        else:
            raise Exception(f'Request failed with status {response.status_code}: {response.text}')

    # Public API Endpoints

    def get_market_summary(self, pair):
        endpoint = f'public/{pair}/marketsummary'
        return self.make_request(endpoint)

    def get_orderbook(self, pair):
        endpoint = f'public/{pair}/orderbook'
        return self.make_request(endpoint)

    def get_trades(self, pair):
        endpoint = f'public/{pair}/trades'
        return self.make_request(endpoint)

    def get_supported_currencies(self):
        endpoint = 'public/currencies'
        return self.make_request(endpoint)

    def get_supported_trading_pairs(self):
        endpoint = 'public/pairs'
        return self.make_request(endpoint)

    # Trading API Endpoints

    def place_order(self, pair, order_type, quantity, price):
        endpoint = 'trade/orders'
        data = {
            'pair': pair,
            'orderType': order_type,
            'quantity': str(quantity),
            'price': str(price)
        }
        return self.make_request(endpoint, method='POST', data=data)

    def cancel_order(self, order_id):
        endpoint = f'trade/orders/{order_id}'
        return self.make_request(endpoint, method='DELETE')

    def get_order_status(self, order_id):
        endpoint = f'trade/orders/{order_id}'
        return self.make_request(endpoint, method='GET')

    def get_order_history(self, pair=None):
        if pair is not None:
            endpoint = f'trade/history/orders/{pair}'
        else:
            endpoint = 'trade/history/orders'
        return self.make_request(endpoint, method='GET')

    def get_trade_history(self, pair=None, order_id=None):
            if pair is not None and order_id is not None:
        raise ValueError('Only one of pair and order_id should be specified')
    elif pair is not None:
        endpoint = f'trade/history/{pair}'
    elif order_id is not None:
        endpoint = f'trade/history/order/{order_id}'
    else:
        endpoint = 'trade/history'
    return self.make_request(endpoint, method='GET')

    def get_fund_deposit_address(self, currency):
        endpoint = f'fund/{currency}/address'
        return self.make_request(endpoint, method='GET')

    def get_fund_balances(self):
        endpoint = 'fund/balances'
        return self.make_request(endpoint, method='GET')

    def get_fund_deposit_history(self, currency=None):
        if currency is not None:
            endpoint = f'fund/history/deposits/{currency}'
        else:
            endpoint = 'fund/history/deposits'
        return self.make_request(endpoint, method='GET')

    def get_fund_withdrawal_history(self, currency=None):
        if currency is not None:
            endpoint = f'fund/history/withdrawals/{currency}'
        else:
            endpoint = 'fund/history/withdrawals'
        return self.make_request(endpoint, method='GET')
